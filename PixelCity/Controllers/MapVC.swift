import UIKit
import MapKit
import CoreLocation
import Alamofire
import AlamofireImage

class MapVC: UIViewController, UIGestureRecognizerDelegate{
    //outlets
    @IBOutlet weak var pullUpView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    //variables
    var localtionManager = CLLocationManager()
    var spinner : UIActivityIndicatorView?
    var progressLabel : UILabel?
    var screenSize = UIScreen.main.bounds
    var collectionView : UICollectionView?
    var flowLayout = UICollectionViewLayout()
    var imageUrlsArray = [String]()
    var imageArray = [UIImage]()
    
    //constants
    let authorizationStatus = CLLocationManager.authorizationStatus()
    let regionRadius : Double = 1000.0
    
    //functions
    override func viewDidLoad() {
        mapView.delegate = self
        localtionManager.delegate = self
        super.viewDidLoad()
        configureLocationServices()
        addDoubleTap()
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
        collectionView?.register(PhotoCell.self, forCellWithReuseIdentifier: "photoCell")
        collectionView?.delegate = self
        collectionView?.dataSource = self
        pullUpView.addSubview(collectionView!)
    }
    func addDoubleTap(){
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(dropPin(_:)))
        doubleTap.numberOfTapsRequired  = 2
        doubleTap.delegate = self
        mapView.addGestureRecognizer(doubleTap)
    }
    func animateViewUp(){
        heightConstraint.constant = 300
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func addSwap(){
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(animateViewDown))
        swipe.direction = UISwipeGestureRecognizerDirection.down
        pullUpView.addGestureRecognizer(swipe)
        
    }
    func addSpinner(){
        spinner = UIActivityIndicatorView()
        spinner?.center = CGPoint(x: (screenSize.width/2) - ((spinner?.frame.width)!/2), y: 150)
        spinner?.activityIndicatorViewStyle = .whiteLarge
        spinner?.color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        spinner?.startAnimating()
        collectionView?.addSubview(spinner!)
    }
    func removeSpinner(){
        if spinner != nil {
            spinner?.removeFromSuperview()
        }
    }
    func addProgressLabel(){
        progressLabel = UILabel()
        progressLabel?.frame = CGRect(x: (screenSize.width/2) - 120, y: 175, width: 240, height: 40)
        progressLabel?.font = UIFont(name: "Avenir Next", size: 18)
        progressLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        progressLabel?.text = "12/40 PHOTOS LOADED"
        progressLabel?.textAlignment = .center
        collectionView?.addSubview(progressLabel!)
    }
    func removeProgressLabel(){
        if progressLabel != nil {
            progressLabel?.removeFromSuperview()
        }
    }
    @objc func animateViewDown(){
        heightConstraint.constant = 0
        cancelAllSessions()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func dropPin(_ sender : UITapGestureRecognizer){
        imageUrlsArray = []
        imageArray = []
        self.collectionView?.reloadData()
        
        removeProgressLabel()
        removeSpinner()
        removePin()
        
        let touchPoint = sender.location(in: mapView)
        let touchCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let annotation = DropablePin(coordinate: touchCoordinate, identifier: "dropablePin")
        mapView.addAnnotation(annotation)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(touchCoordinate, regionRadius*2.0, regionRadius*2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        
        animateViewUp()
        addSwap()
        addSpinner()
        addProgressLabel()
        cancelAllSessions()
        
        retrieveUrls(forAnnotation: annotation) { (status) in
            if status {
                self.retrieveImages(completion: { (success) in
                    self.removeSpinner()
                    self.removeProgressLabel()
                    self.collectionView?.reloadData()
                })
            }
        }

    }
    func removePin(){
        for annotation in  mapView.annotations {
            mapView.removeAnnotation(annotation)
        }
    }
    @IBAction func centerMapButtonPressed(_ sender: Any) {
        if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
            centerMapOnUserLocation()
        }
    }
    
    func retrieveUrls(forAnnotation annotation : DropablePin, completion : @escaping CompletionHandler){
        Alamofire.request(getUrl(forApiKet: API_KEY, withAnnotation: annotation, andNumberOfPhotos: 40)).responseJSON { (response) in
            if response.result.error == nil {
                guard let json = response.result.value as? Dictionary<String, AnyObject> else {return}
                if let photosDic = json["photos"] as? Dictionary<String, AnyObject> {
                    if let photosDicArray = photosDic["photo"] as? [Dictionary<String,AnyObject>] {
                        for photo in photosDicArray {
                            let url = "https://farm\(photo["farm"]!).staticflickr.com/\(photo["server"]!)/\(photo["id"])_\(photo["sercret"]!)_h_d.jpg"
                            //TODO : fix photo id, idk but ça bug lol
                            self.imageUrlsArray.append(url)
                            completion(true)
                        }
                    }
                }
            }else{
                completion(false)
            }
        }
    }
    
    func retrieveImages(completion : @escaping CompletionHandler){
        for url in imageUrlsArray {
            Alamofire.request(url).responseImage(completionHandler: { (response) in
                if response.result.error == nil {
                    guard let image = response.result.value else {return}
                    self.imageArray.append(image)
                    self.progressLabel?.text = "\(self.imageArray.count)/40 LOADED"
                }else{
                    completion(false)
                }
                if self.imageArray.count == self.imageUrlsArray.count {
                     completion(true)
                }
            })
        }
    }
    
    func cancelAllSessions(){
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach({$0.cancel()})
            downloadData.forEach({$0.cancel()})
        }
    }
}

extension MapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        let pinAnnotion = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "dropablePin")
        pinAnnotion.pinTintColor = #colorLiteral(red: 1, green: 0.6331872344, blue: 0, alpha: 1)
        pinAnnotion.animatesDrop = true
        return pinAnnotion
    }
    
    func centerMapOnUserLocation(){
        guard let coordinate = localtionManager.location?.coordinate else {return}
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, regionRadius*2.0, regionRadius*2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}

extension MapVC: CLLocationManagerDelegate {
    func configureLocationServices(){
        if authorizationStatus == .notDetermined {
            localtionManager.requestAlwaysAuthorization()
        }else{
            return
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMapOnUserLocation()
    }
}

extension MapVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArray.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? PhotoCell else {return UICollectionViewCell()}
        let imageFromIndex = imageArray[indexPath.row]
        let imageView = UIImageView(image: imageFromIndex)
        cell.addSubview(imageView)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopVC") as? PopVC else {return}
        popVC.setUpView(forImage: imageArray[indexPath.row])
        present(popVC, animated: true) {
            print("YAY")
        }
    }
}
