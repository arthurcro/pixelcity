//
//  PopVC.swift
//  PixelCity
//
//  Created by Arthur Crocquevieille on 31/10/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import UIKit

class PopVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var popImage: UIImageView!
    var passedImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        popImage.image = passedImage
        addDoubleTap()
    }
    func setUpView(forImage image : UIImage){
        passedImage = image
    }
    func addDoubleTap(){
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(hidePopVC))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        view.addGestureRecognizer(doubleTap)
    }
    @objc func hidePopVC(){
        dismiss(animated: true, completion: nil)
    }

}
