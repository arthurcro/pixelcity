
//
//  dropabePin.swift
//  PixelCity
//
//  Created by Arthur Crocquevieille on 28/10/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DropablePin : NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    var identifier : String
    
    init(coordinate : CLLocationCoordinate2D, identifier : String) {
        self.coordinate = coordinate
        self.identifier = identifier
        super.init()
    }
}
