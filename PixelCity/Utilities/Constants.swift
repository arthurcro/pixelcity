//
//  Constants.swift
//  PixelCity
//
//  Created by Arthur Crocquevieille on 29/10/2017.
//  Copyright © 2017 Arthur Crocquevieille. All rights reserved.
//

import Foundation

//Complition handler
typealias CompletionHandler = (_ status : Bool )->()

//API KET
let API_KEY = "903fb47b981b71bcaa600362da5fe8a2"

func getUrl (forApiKet key : String, withAnnotation annotation : DropablePin, andNumberOfPhotos number : Int)-> String {
    
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(key)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}
